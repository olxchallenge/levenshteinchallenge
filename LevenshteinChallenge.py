#!/usr/bin/python
__author__      = "Frederico Dias Wu"
__copyright__   = "Copyright 2016, The Challenge"
__license__     = "GPL"
__version__     = "1.0"
__email__       = "fredericowu@gmail.com"
__status__      = "Production"


import logging
import sys

STORED_WORDS = [ "banana", "abacate" ]
THRESHOLD = 3
DEBUG = False

logging.basicConfig(stream=sys.stdout)
LOG = logging.getLogger('logger')

def Distance(word1, word2, index = 0, distance = 0):
  # Begin with highest length word
  if len(word2) > len(word1):
    return Distance(word2, word1)
  else:
    if word1 == word2:
      return distance
    else:
      if word1[index] <> word2[index]:
        distance = distance + 1
        word2_up = word2[:index] + word1[index] + word2[index + 1:]

        # Checking if inserting/updating character
        if len(word1) > len(word2):
          # inserting
          word2_ins = word2[:index] + word1[index] + word2[-len(word2)+index:]
          distance_ins = Distance(word1, word2_ins, index + 1, 0)

          # updating
          if index < len(word2) - 1:
            distance_up = Distance(word1, word2_up, index + 1, 0)
          else:
            # not possible to update non existing index, setting the highest depth
            distance_up = len(word1)

          # Choose shortest distance
          word2 = word2_ins if distance_ins < distance_up else word2_up

        # Updating only way
        else:
          word2 = word2_up

      return Distance(word1, word2, index + 1, distance)



def check_distance(word1, word2, threshold):
  distance = Distance(word1, word2)
  ok = True if distance <= threshold else False

  LOG.debug("%s x %s = %d -> %s" % ( word1, word2, distance, str(ok) ))

  return ok


def check_word_list(word_list, word, threshold):
  for item in word_list:
    if check_distance(item, word, threshold):
      print "%s / %s" % (item, word)
 

def main(argv):
  import argparse

  parser = argparse.ArgumentParser( description='Levenshtein Distance Algorithm by Frederico Wu')
  parser.add_argument('-w','--word-list', help='Word list separated by "," (comma). Example: banana,abacate', required=False, default = ",".join( STORED_WORDS ) )
  parser.add_argument('-t','--threshold', help='Threshold of the distance', type=int, required=False, default = THRESHOLD )
  parser.add_argument('-d','--debug', help='Threshold of the distance', required=False, action='store_true' ) 
  parser.add_argument('-c','--check-word',help='Check the distance of this word and the others in the word list', required=True )

  if not argv:
    parser.print_help()
    print "\nExample: %s -w banana -c abacate\n" % ( sys.argv[0] )
  else:
    args = parser.parse_args()
    DEBUG = args.debug
    LOG.setLevel(DEBUG)

    check_word_list( args.word_list.split(","), args.check_word, args.threshold )

if __name__ == '__main__':
  import sys

  main( sys.argv[1:] )


#print check_distance(w1, w2, 3)

#print levenshteinDistance(w1, w2)
#print "-----------"
#print Distance(w1, w2)




