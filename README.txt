ABOUT

This is the Levenshtein Challenge built in Python language by Frederico Wu.


REQUIREMENTS

This script was tested with Python 2.7.


USAGE

To try the script you may execute the commands below.

python LevenshteinChallenge.py -w banana,abacate -c bacana -t 4
python LevenshteinChallenge.py -c banana -d

You may also list the options by executing:
python LevenshteinChallenge.py -h


The word list may be specified by command as shown
